<?php

namespace Drupal\flag_translation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagService;

/**
 * Translations specific overrides for Flag service.
 */
class FlagTranslationService extends FlagService {

  /**
   * {@inheritDoc}
   */
  public function flag(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {

    $flagging = $this->getFlagging($flag, $entity, $account, $session_id);

    if (is_null($flagging)) {
      $flagging = parent::flag($flag, $entity, $account, $session_id);
    }

    // @todo Make this hard-coded value configurable.
    if ($flag->id() == 'content_editor') {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $this->addTranslationFlagging($flagging->id(), $language);
    }

    return $flagging;

  }

  /**
   * {@inheritdoc}
   */
  public function unflag(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $flagging = $this->getFlagging($flag, $entity, $account, $session_id);

    $this->removeTranslationFlagging($flagging->id(), $language);
    if (!$this->translationFlaggingsExist($flagging->id())) {
      parent::unflag($flag, $entity, $account, $session_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFlaggingUsers(EntityInterface $entity, FlagInterface $flag = NULL) {
    if ($entity->getEntityTypeId() != 'node') {
      return parent::getFlaggingUsers($entity, $flag);
    }

    $query = $this->entityTypeManager->getStorage('flagging')->getQuery();
    $query->accessCheck(TRUE);
    $query->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id());

    if (!empty($flag)) {
      $query->condition('flag_id', $flag->id());
    }

    $ids = $query->execute();
    // Load the flaggings.
    $flaggings = $this->getFlaggingsByIds($ids);

    $user_ids = [];
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    foreach ($flaggings as $flagging) {
      if ($this->translationFlaggingsExist($flagging->id(), $language)) {
        $user_ids[] = $flagging->get('uid')->first()->getValue()['target_id'];
      }
    }

    return $this->entityTypeManager->getStorage('user')
      ->loadMultiple($user_ids);

  }

  /**
   * Add translation flagging.
   *
   * @param int $flagging_id
   *   Flagging ID.
   * @param string $langcode
   *   Translation being flagged.
   */
  private function addTranslationFlagging(int $flagging_id, string $langcode) {
    $query = \Drupal::database()->select('flag_translation', 'ft');
    $items = $query->fields('ft', ['id'])
      ->condition('flagging_id', $flagging_id)
      ->condition('langcode', $langcode)
      ->execute()
      ->fetchCol();

    if (empty($items)) {
      $query = \Drupal::database()->insert('flag_translation')->fields([
        'flagging_id',
        'langcode',
      ])
        ->values([
          $flagging_id,
          $langcode,
        ])
        ->execute();
    }

  }

  /**
   * Check if there are any translation flaggings.
   *
   * @param int $flagging_id
   *   Flagging ID.
   * @param string|null $langcode
   *   Optional language parameter.
   *
   * @return bool
   *   TRUE if at least one translation is flagged, FALSE otherwise.
   */
  public function translationFlaggingsExist(int $flagging_id, string $langcode = NULL): bool {
    $query = \Drupal::database()->select('flag_translation', 'ft');
    $query->fields('ft', ['id']);

    if (!is_null($langcode)) {
      $query->condition('langcode', $langcode);
    }

    $items = $query
      ->condition('flagging_id', $flagging_id)
      ->execute()
      ->fetchCol();
    return !empty($items);
  }

  /**
   * Remove translation flagging.
   *
   * @param int $flagging_id
   *   Flagging ID.
   * @param string $langcode
   *   Translation being flagged.
   */
  private function removeTranslationFlagging(int $flagging_id, string $langcode) {
    \Drupal::database()->delete('flag_translation')
      ->condition('flagging_id', $flagging_id)
      ->condition('langcode', $langcode)
      ->execute();
  }

}
