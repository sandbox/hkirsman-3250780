<?php

namespace Drupal\flag_translation\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\Entity\Flag;

/**
 * Override of default Flag entity class.
 */
class SmartFlag extends Flag {

  /**
   * Check if entity is flagged or not.
   *
   * This default isFlagged method override does additional checks to see if
   * content translation user is currently viewing is flagged or not. This
   * information is stored in flag_translation database table provided by this
   * module. There are other overrides that allow storing data there in addition
   * to the default flagging table -
   * \Drupal\flag_translation\FlagTranslationService::flag() and
   * \Drupal\flag_translation\FlagTranslationService::unflag()
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity being flagged.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   Account object.
   * @param string|null $session_id
   *   Session ID.
   *
   * @return bool
   *   TRUE if current translation is flagged, FALSE otherwise.
   */
  public function isFlagged(EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL): bool {
    if ($entity->id() === NULL) {
      return FALSE;
    }

    // Custom overrides are only valid for content editor notifications flag.
    // Depending on the requirements this might change in the future.
    // @todo Make this hard-coded value configurable.
    if ($this->id() != 'content_editor') {
      return parent::isFlagged($entity, $account, $session_id);
    }

    $flagging_exists = parent::isFlagged($entity, $account, $session_id);

    $language = $this->languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\flag\FlagService $flag_service */
    $flag_service = \Drupal::service('flag');

    $flagging = $flag_service->getFlagging($this, $entity, $account, $session_id);
    if (is_null($flagging)) {
      return FALSE;
    }

    // Check if in addition to the node flagging there is an entry about
    // specific translation flagging.
    $translation_flagging_exists = $flag_service->translationFlaggingsExist($flagging->id(), $language);

    // There is no point in checking anything else of node flagging does not
    // exist.
    if ($flagging_exists == TRUE) {
      // If in addition to the node flagging there is also an entry about
      // translation specific flagging then we can return TRUE.
      if ($translation_flagging_exists) {
        return TRUE;
      }
      // If node flagging exists, but this particular translation has not been
      // flagged then we can return FALSE.
      else {
        return FALSE;
      }
    }

    return FALSE;

  }

}
