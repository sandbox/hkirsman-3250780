<?php

declare(strict_types=1);

namespace Drupal\flag_translation;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Override services.
 */
class FlagTranslationServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    parent::alter($container);
    $container->getDefinition('flag')
      ->setClass(FlagTranslationService::class);
  }

}
